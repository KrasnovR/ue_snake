// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "UE_SnakeGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class UE_SNAKE_API AUE_SnakeGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
