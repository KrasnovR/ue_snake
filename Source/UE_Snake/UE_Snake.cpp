// Copyright Epic Games, Inc. All Rights Reserved.

#include "UE_Snake.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, UE_Snake, "UE_Snake" );
